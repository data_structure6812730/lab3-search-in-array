public class Solution {
        public int mySqrt(int x) {
            if (x == 0 || x == 1) {
                return x;
            }
            
            long left = 1;
            long right = x;
            long result = 0;
            
            while (left <= right) {
                long mid = left + (right - left) / 2;
                long square = mid * mid;
                
                if (square == x) {
                    return (int) mid;
                } else if (square < x) {
                    left = mid + 1;
                    result = mid; // Store the potential result before moving right
                } else {
                    right = mid - 1;
                }
            }
            
            return (int) result;
        }
        
        public static void main(String[] args) {
            Solution solution = new Solution();
            
            System.out.println(solution.mySqrt(4)); // Output: 2
            System.out.println(solution.mySqrt(8)); // Output: 2
        }
    }
    

